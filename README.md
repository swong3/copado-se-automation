# Copado SE Automation

SE Automation to automate provision of trial orgs using CRT.

## Information
CRT integrates with Git at the Test Suite level. i.e. each Test Suite is mapped to a git repo branch.

As I have broken down the entire automation into individual suites with each suite automating one part of the setup, each branch in this repo will map to a test suite. 

The main branch contains the common.robot and locators.robot which are common resources shared by all the test suites. As such every time these files are updated, make sure to merge it into the main branch and pull them into the individual branches.

Branches:
- main - common.robot and locators.robot
- step-1-setup-org - step-1-setup-org.robot 
- step-2-create-users - step-2-create-users.robot
- step-3-create-sandboxes - step-3-create-sandboxes.robot
- step-4-install-copado - step-4-install-copado.robot
- step-4.5-register-application - step-4.5-register-application.robot (this step was added so that if we needed to change the Copado Backend URL, we can do so before running this script)
- step-5-create-credentials - step-5-create-credentials.robot
- step-6-create-production-git-snapshot - step-6-create-prod-git-snapshot.robot
- step-7-create-other-git-snapshots - step-7-create-other-git-snapshots.robot
- step-8-setup-sca-settings - step-8-setup-sca-settings.robot (Work in progress and not ready)
- step-9-create-pipeline - step-9-create-pipeline.robot (Work in progress and not ready)

By default, the script will create 2 Dev, 1 SIT, 1 UAT, 1 Hotfix sandboxes
By default, the credentials are named <Environment>_MAIN. E.g. DEV1_MAIN, SIT_MAIN, PROD_MAIN


## Set Up Instructions
0. Clone this repo
1. Get a CRT account at https://robotic.copado.com or https://eu-robotic.copado.com
2. Create a Project
3. Create a Robot
4. Create a Test Suite and link to the newly cloned repo and branch
5. Repeat step 4 for every branch
6. At the Robot, level you will need to create the following variables:
    1. sfdomain - the domain of your Salesforce org
    2. sfuser - the username to log into your Salesforce.org
    3. sfpassword - the password to your user
    4. package_ver - the package version of Copado Deployer that you want to install
    5. package_id - the package id of Copado Deployer that you want to install
    6. git_repo - the name of the git repository record that you have set up in Copado.




## Getting started with Gitlab

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/swong3/copado-se-automation.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/swong3/copado-se-automation/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
