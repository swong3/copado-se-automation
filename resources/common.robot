*** Settings ***
Library                    QWeb
Library                    QForce
Library                    String


*** Variables ***
${BROWSER}               chrome
${login_url}             https://${sfdomain}.my.salesforce.com/            # Salesforce instance. NOTE: Should be overwritten in CRT variables
${home_url}              ${login_url}/lightning/page/home


*** Keywords ***
Setup Browser
    Open Browser          about:blank                 ${BROWSER}
    Set Library Search Order    QForce      QWeb
    SetConfig             LineBreak                   ${EMPTY}               #\ue000
    SetConfig             DefaultTimeout              20s                    #sometimes salesforce is slow
    Login

End suite
    Close All Browsers

Login
    [Documentation]      Login to Salesforce instance
    GoTo                 ${login_url}
    TypeText             Username                    ${sfuser}
    TypeSecret           Password                    ${sfpassword}
    ClickText            Log In


Home
    [Documentation]      Navigate to homepage, login if needed
    GoTo                 ${home_url}
    ${login_status} =    IsText                      To access this page, you have to log in to Salesforce.    2
    Run Keyword If       ${login_status}             Login
    VerifyText           Home
